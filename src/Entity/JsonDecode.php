<?php


namespace App\Entity;

use App\Entity\CentreTri;
use App\Entity\Compost;
use App\Entity\GlassRecycle;
use App\Entity\Incinerator;
use App\Entity\MetalRecycle;
use App\Entity\PaperRecycle;
use App\Entity\PlasticRecycle;
use App\Entity\Wastes;


class JsonDecode
{
    public function jsonDecodeWaste()
    {
        //récupérer les informations
        $json = file_get_contents('./data.json');
        $parseJson = json_decode($json, true);

        //assignation du parseJson avec la les variables
        $quartiers = $parseJson['quartiers'];

        //boucle pour obtenir tout les déchets par catégorie
        foreach ($quartiers as $quartier) {
            $paperWaste = new Wastes('papier', $quartier['papier']);
            var_dump($paperWaste);
            $organicWaste = new Wastes('organique', $quartier['organique']);
            var_dump($organicWaste);
            $glassWaste = new Wastes('verre', $quartier['verre']);
            var_dump($glassWaste);
            $metalWaste = new Wastes('metaux', $quartier['metaux']);
            var_dump($metalWaste);
            $otherWaste = new Wastes('autre', $quartier['autre']);
            var_dump($otherWaste);
            $plasticPetWaste = new Wastes('plastiques', $quartier['plastiques']['PET']);
            var_dump($plasticPetWaste);
            $plasticPvcWaste = new Wastes('plastiques', $quartier['plastiques']['PVC']);
            var_dump($plasticPvcWaste);
            $plasticPcWaste = new Wastes('plastiques', $quartier['plastiques']['PC']);
            var_dump($plasticPcWaste);
            $plasticPehdWaste = new Wastes('plastiques', $quartier['plastiques']['PEHD']);
            var_dump($plasticPehdWaste);
        }
    }

    public function jsonDecodeServices()
    {
        //récupérer les informations
        $json = file_get_contents('./data.json');
        $parseJson = json_decode($json, true);

        //assignation du parseJson avec la les variables
        $services = $parseJson['services'];

        //boucle pour obtenir tout les services
        foreach ($services as $service) {
            switch ($service['type']) {
                case 'composteur':
                    $composteur = new Compost ($service['type'], $service['capacite'], ['organique']);
                    var_dump($composteur);
                    break;
                case 'incinerateur':
                    //calcul pour obtenir la capacité 
                    $capacity = floatval(['ligneFour' * 'capaciteLigne']);
                    $incinerator = new Incinerator($service['type'], $capacity);
                    var_dump($incinerator);
                    break;
                case 'recyclagePapier':
                    $paperRecycle = new PaperRecycle($service['type'], $service['capacite'], ['papier']);
                    var_dump($paperRecycle);
                    break;
                case 'recyclagePlastique':
                    $plasticRecycle = new PlasticRecycle ($service['type'], $service['capacite'], []);
                    var_dump($plasticRecycle);
                    break;
                case 'recyclageMetaux':
                    $metalRecycle = new MetalRecycle($service['type'], $service['capacite'], []);
                    var_dump($metalRecycle);
                    break;
                case 'recyclageVerre':
                    $glassRecycle = new GlassRecycle($service['type'], $service['capacite'], []);
                    var_dump($glassRecycle);
                    break;
                case 'centreTri':
                    $sortingCenter = new CentreTri($service['type'], $service['capacite'],[]);
                    var_dump($sortingCenter);
                    break;
            }
        }
    }
}
