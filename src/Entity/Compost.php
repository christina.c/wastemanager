<?php

namespace App\Entity;

//fonctions dans abstractCenter
class Compost extends AbstractCenter
{
    public $name;
    public $capacity;
    public $wasteType;

    public function __construct(string $name, float $capacity)
    {
      parent::__construct($name, $capacity, [], []);
        $this->wasteType = ['organique'];
    }
}