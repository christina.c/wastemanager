<?php

namespace App\Entity;

//fonctions dans abstractCenter
class PlasticRecycle extends AbstractCenter
{
    public $name;
    public $capacity;
    public $wasteType;

    public function __construct(string $name, float $capacity, array $wasteType)
    {
      parent::__construct($name, $capacity, [], []);
        $this->wasteType = $wasteType;
    }
}