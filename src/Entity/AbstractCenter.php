<?php

namespace App\Entity;


abstract class AbstractCenter
{

    public $name;
    public $capacity;
    public $wasteType;


    public function __construct(string $name, float $capacity, array $wasteType)
    {
        $this->name = $name;
        $this->capacity = $capacity;
        $this->wasteType = $wasteType;
    }

    public function treatment(Wastes $waste)
    {
        if ($this->weight + $waste->Weight() <= $this->capacity) {
            array_push($this->wasteType, $waste);
            $this->weight += $waste->Weight();
        } else {
            array_push($this->otherWaste, $waste);
        }
    }

    public function getWasteType() : array
    {
        return $this->wasteType;
    }

    public function getWeight() : float
    {
        return $this->weight;
    }

    public function getCapacity() : float
    {
        return $this->capacity;
    }

    public function getWasteLeft() : array
    {
        return $this->wasteLeft;
    }            
}