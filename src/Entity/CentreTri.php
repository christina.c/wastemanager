<?php

namespace App\Entity;


class CentreTri
{
    public $name;
    public $capacity;
    public $wasteType;

    public function __construct(string $name, float $capacity, array $wasteType)
    {
        $this->name = $name;
        $this->capacity = $capacity;
        $this->wasteType = $wasteType;
    }

    public function getCapacity(){
        
        return $this->capacity;
    } 

    public function getComposteurCapacity() {
        
    }
}