<?php

namespace App\Entity;


class Wastes
{
    private $name;
    private $weight;

    public function __construct(string $name, float $weight)
    {
        $this->name = $name;
        $this->weight = $weight;
    }

    public function weight() : float
    {
        return $this->weight;
    }
}
