<?php

use App\Entity\Compost;
use App\Entity\JsonDecode;

require __DIR__ . '/../vendor/autoload.php';

$jsonWaste = new JsonDecode();
$jsonWaste->jsonDecodeWaste($paperWaste);

$jsonService = new JsonDecode();
$jsonWaste->jsonDecodeServices($composteur);

$compost = new Compost('organique', 0);
$compost->treatment($waste);
